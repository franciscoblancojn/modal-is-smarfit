<?php
require_once(preg_replace('/wp-content.*$/','',__DIR__).'wp-load.php');
$data = json_decode(file_get_contents('php://input'), true);
if(isset($data)){
    $_POST = $data;
}

$error = false;
//validate $_POST
if(!isset($_POST['dni']) || $_POST['dni'] == null || $_POST['dni'] == ""){
    $error = array(
        "type"  => "error",
        "msj"   => "DNI Requerido",
    );
}
if(!isset($_POST['password']) || $_POST['password'] == null  || $_POST['password'] == ""){
    $error = array(
        "type"  => "error",
        "msj"   => "Password Requerido",
    );
}
if(!isset($_POST['email']) || $_POST['email'] == null || $_POST['email'] == ""){
    $error = array(
        "type"  => "error",
        "msj"   => "Email Requerido",
    );
}
if($error!==false){
    echo json_encode($error);
    exit;
}


function MISP_isSmargit(string $login) {
    //new request login
    $curl = curl_init();

    curl_setopt_array($curl, array(
    CURLOPT_URL => MISP_urlApi,
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING => "",
    CURLOPT_MAXREDIRS => 10,
    CURLOPT_TIMEOUT => 0,
    CURLOPT_FOLLOWLOCATION => true,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => "POST",
    CURLOPT_POSTFIELDS => array(
        'login' => $login,
        'authentication_field' => 'client_token',
        'authentication_value' => MISP_auth
    ),
    CURLOPT_HTTPHEADER => array(
        "authorization: ".MISP_authToken
    ),
    ));

    $response = curl_exec($curl);
    $statusCode = curl_getinfo($curl, CURLINFO_HTTP_CODE); 
    curl_close($curl);

    $response = json_decode($response);

    if($response->errors == "invalid_login"){
        return array(
            "type"  =>  "Error",
            "error" =>  "invalid_login"
        );
    }else{
        $token = $response->auth_token;
        
        $curl = curl_init();
        curl_setopt_array($curl, array(
        CURLOPT_URL => MISP_urlApi."/".$token,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "GET",
        CURLOPT_HTTPHEADER => array(
            "authorization: ".MISP_authToken
        ),
        ));

        $response = curl_exec($curl);
        $statusCode = curl_getinfo($curl, CURLINFO_HTTP_CODE); 

        curl_close($curl);

        $response = json_decode($response);
        if($response->errors == "invalid_single_access_token"){
            return array(
                "type"  => "Error",
                "error" => "invalid_single_access_token"
            );
        }else{
            //TODO: plan is adon comente
            if(
                $response->status == "active" 
                // && 
                // (
                //     $response->plan == "Digital" ||
                //     $response->plan == "Smart" ||
                //     $response->plan == "Black" ||
                //     strrpos(strtoupper($s) , 'VIP') === 0
                // )
                ){
                return array(
                    'type' => "ok", 
                    'payload' => array(
                        'auth_token' => $token, 
                    ),
                );
            }else{
                return array(
                    "type"  => "Error",
                    "error" => "status no active",
                );
            }
        }
    }
    return;
}
$r = MISP_isSmargit($_POST["dni"]);
$user = get_user_by( 'email',$_POST['email'] );
if($user === false){
    wp_create_user( $_POST['dni'], $_POST['password'], $_POST['email'] );
}
$user = get_user_by( 'email',$_POST['email'] );
$authenticate = wp_authenticate( $user->data->user_login , $_POST['password'] );

if(is_wp_error($authenticate)){
    $error = array(
        "type"  => "error",
        "msj"   => "",
    );
    foreach ($authenticate->errors as $key => $value) {
        $error['msj'].="$value[0]<br/>";
    }
    echo json_encode($error);
    exit;
}

$user_id = $user->data->ID;
$creds = array(
    'user_login'    => $user->data->user_login ,
    'user_password' => $_POST['password'],
    'remember'      => true
);
wp_signon( $creds, false );
wp_clear_auth_cookie();
wp_set_current_user ( $user->ID , $user->data->user_login );
wp_set_auth_cookie  ( $user->ID );

$urlRediret = $_POST['urlRediret'];
$urlRediretIsSmarfit = $_POST['urlRediretIsSmarfit'];

if($r["type"] == "ok"){
    update_user_meta($user_id,"dni",$_POST["dni"]);
    update_user_meta($user_id,"isUserSmarfit","true");
    delete_user_meta($user_id,"suscription");
    $result = array(
        "type"      => "ok",
        "isUserSmarfit"   => "true"
    );
    echo json_encode($result);
}else{
    $result = array(
        "type"      => "ok",
        "isUserSmarfit"   => "false",
        "result" => $r
    );
    echo json_encode($result);
}