<button class="btnModal" onclick="showModal()">
    <?=$settings['textButtonModal']?>
</button>
<div id="MISP" class="MISP" hidden>
    <div  class="form">
        <h1>
            <?=$settings['textTitle']?>
        </h1>
        <h3 id="MISP_error" class="error"></h3>
        <label for="dni">
            <input 
                id="dni" 
                name="dni" 
                type="text"
                class="input"
                placeholder="<?=$settings['paceholder_dni']?>"
                required
            />
        </label>
        <label for="email">
            <input 
                id="email" 
                name="email" 
                type="email"
                class="input"
                placeholder="<?=$settings['paceholder_email']?>"
                required
            />
        </label>
        <label for="password">
            <input 
                id="password" 
                name="password" 
                type="password"
                class="input"
                placeholder="<?=$settings['paceholder_password']?>"
                required
            />
        </label>
        <p>
            <?=$settings['textExpli']?>
        </p>
        <input 
            onclick="actionMISP()"
            value="<?=$settings['textButton']?>"
            class="submit"
            type="submit"
        />
        <input 
            onclick="hiddenModal()"
            value="Cancelar"
            class="submit"
            type="submit"
            style="background:#D52B1E;"
        />
    </div>
</div>
<script>
    urlRediret = "<?=$settings['urlRediret']?>";
    urlRediretIsSmarfit = "<?=$settings['urlRediretIsSmarfit']?>";
    MISP_url_action = `<?=MISP_url?>/action/isSmarfitLogin.php`;
</script>
<script src="<?=MISP_url?>widget/js/modalIsSmarfit.js" onload="isHiddenModal();"></script>
<style>
.MISP{
    position:fixed;
    top:0;
    left:0;
    width:100%;
    height:100%;
    z-index:999999999999;
    background:#000;
    display:flex;
    justify-content:center;
    align-items:center;
    padding:10px;
    font-family: "Gotham Book", Sans-serif;
}
<?php
//if(!(\Elementor\Plugin::$instance->editor->is_edit_mode())){
?>
.MISP[hidden]{
    display:none !important;
}
<?php
//}
?>
.MISP .form{
    width:100%;
    max-width:400px;
    background:#fff;
    border-radius:5px;
    padding:20px 10px;
    text-align:center;
}
.MISP label,
.MISP .input{
    display:block;
    width:100%;
}
.MISP .input{
    margin-bottom:15px;
}
.MISP .error{
    color:red;
}
</style>
