hiddenModal = () => {
    localStorage.setItem("hiddenModal","true")
    document.getElementById('MISP').hidden = true;
}
showModal = () => {
    document.getElementById('MISP').hidden = false;
}
isHiddenModal = () => {
    return;
    if(localStorage.hiddenModal=="true"){
        document.getElementById('MISP').hidden = true;
    }
}
actionMISP = () => {
    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");

    dni = document.getElementById('dni')
    email = document.getElementById('email')
    password = document.getElementById('password')

    var raw = JSON.stringify({
        "dni":dni.value,
        "email":email.value,
        "password":password.value
    });

    var requestOptions = {
    method: 'POST',
    headers: myHeaders,
    body: raw,
    redirect: 'follow'
    };

    fetch(MISP_url_action, requestOptions)
    .then(response => response.text())
    .then(result => {
        result = JSON.parse(result)
        console.log(result)
        if (result.type=="error") {
            error = document.getElementById('MISP_error')
            error.innerHTML = result.msj
        }else{
            hiddenModal()
            if (result.isUserSmarfit == "true") {
                window.location = urlRediretIsSmarfit
            }else{
                window.location = urlRediret
            }
        }
    })
    .catch(error => console.log('error', error));
}
window.addEventListener('load', function(){ isHiddenModal() } );