<?php
MISP_add_control_style( "modal" , ".MISP" , $this , array(
    'padding'       => true,
    'border'        => true,
    'color'         => true,
    'background'    => true,
    'typography'    => true,
    'borderRadius'  => true,
) , 'Modal');
MISP_add_control_style( "btnModal" , ".btnModal" , $this , array(
    'padding'       => true,
    'border'        => true,
    'color'         => true,
    'background'    => true,
    'typography'    => true,
    'borderRadius'  => true,
) , 'Bottom de Modal');
MISP_add_control_style( "title" , "h1" , $this , array(
    'padding'       => true,
    'border'        => true,
    'color'         => true,
    'background'    => true,
    'typography'    => true,
    'borderRadius'  => true,
) , 'Title');
MISP_add_control_style( "form" , ".form" , $this , array(
    'padding'       => true,
    'border'        => true,
    'background'    => true,
    'margin'        => true,
    'borderRadius'  => true,
) , 'Form');
MISP_add_control_style( "label" , "label" , $this , array(
    'padding'       => true,
    'border'        => true,
    'background'    => true,
    'margin'        => true,
    'borderRadius'  => true,
) , 'Label');
MISP_add_control_style( "input" , ".input" , $this , array(
    'hover'         => true,
    'padding'       => true,
    'border'        => true,
    'color'         => true,
    'background'    => true,
    'typography'    => true,
    'margin'        => true,
    'borderRadius'  => true,
) , 'Input');
MISP_add_control_style( "submit" , ".submit" , $this , array(
    'hover'         => true,
    'padding'       => true,
    'border'        => true,
    'color'         => true,
    'background'    => true,
    'typography'    => true,
    'margin'        => true,
    'borderRadius'  => true,
) , 'Submit');