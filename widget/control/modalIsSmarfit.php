<?php
$this->start_controls_section(
    'configModal',
    [
        'label'     => __( 'Modal' ),
        'tab'       => \Elementor\Controls_Manager::TAB_CONTENT,
    ]
);
$this->add_control(
    'textButtonModal',
    [
        'label'         => __( 'Texto del Botton' ),
        'type'          => \Elementor\Controls_Manager::TEXT,
        'default'       => __( 'Eres Usuario SmarfitGo' ),
    ]
);
$this->end_controls_section();
$this->start_controls_section(
    'configTitle',
    [
        'label'     => __( 'Title' ),
        'tab'       => \Elementor\Controls_Manager::TAB_CONTENT,
    ]
);
$this->add_control(
    'textTitle',
    [
        'label'         => __( 'Texto' ),
        'type'          => \Elementor\Controls_Manager::TEXT,
        'default'       => __( 'Eres Usuario SmarfitGo' ),
    ]
);
$this->end_controls_section();
$this->start_controls_section(
    'configDNI',
    [
        'label'     => __( 'DNI' ),
        'tab'       => \Elementor\Controls_Manager::TAB_CONTENT,
    ]
);
$this->add_control(
    'paceholder_dni',
    [
        'label'         => __( 'Placeholder' ),
        'type'          => \Elementor\Controls_Manager::TEXT,
        'default'       => __( 'DNI' ),
    ]
);
$this->end_controls_section();
$this->start_controls_section(
    'configEmail',
    [
        'label'     => __( 'Email' ),
        'tab'       => \Elementor\Controls_Manager::TAB_CONTENT,
    ]
);
$this->add_control(
    'paceholder_email',
    [
        'label'         => __( 'Placeholder' ),
        'type'          => \Elementor\Controls_Manager::TEXT,
        'default'       => __( 'Email' ),
    ]
);
$this->end_controls_section();

$this->start_controls_section(
    'configPassword',
    [
        'label'     => __( 'Password' ),
        'tab'       => \Elementor\Controls_Manager::TAB_CONTENT,
    ]
);
$this->add_control(
    'paceholder_password',
    [
        'label'         => __( 'Placeholder' ),
        'type'          => \Elementor\Controls_Manager::TEXT,
        'default'       => __( 'Password' ),
    ]
);
$this->end_controls_section();

$this->start_controls_section(
    'configButtom',
    [
        'label'     => __( 'Button' ),
        'tab'       => \Elementor\Controls_Manager::TAB_CONTENT,
    ]
);
$this->add_control(
    'textButton',
    [
        'label'         => __( 'Text' ),
        'type'          => \Elementor\Controls_Manager::TEXT,
        'default'       => __( 'Enviar' ),
    ]
);
$this->end_controls_section();

$this->start_controls_section(
    'configTextExpli',
    [
        'label'     => __( 'Texto Adicional' ),
        'tab'       => \Elementor\Controls_Manager::TAB_CONTENT,
    ]
);
$this->add_control(
    'textExpli',
    [
        'label'         => __( 'Texto' ),
        'type'          => \Elementor\Controls_Manager::TEXT,
        'default'       => __( 'Si no has creado usuario se crea automaticamente con Email y Password' ),
    ]
);
$this->end_controls_section();

$this->start_controls_section(
    'configRediret',
    [
        'label'     => __( 'Url Rediret no Smarfit' ),
        'tab'       => \Elementor\Controls_Manager::TAB_CONTENT,
    ]
);
$this->add_control(
    'urlRediret',
    [
        'label'         => __( 'Url checkout' ),
        'type'          => \Elementor\Controls_Manager::TEXT,
        'default'       => __( '/checkout/?add-to-cart=3516' ),
    ]
);
$this->add_control(
    'urlRediretIsSmarfit',
    [
        'label'         => __( 'Url rediret is Smarfir' ),
        'type'          => \Elementor\Controls_Manager::TEXT,
        'default'       => __( '/smartfit-go' ),
    ]
);
$this->end_controls_section();